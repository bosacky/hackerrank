use std::collections::{BTreeMap, VecDeque};
use std::cmp::PartialOrd;

struct MonotonicQueue<T: PartialOrd> {
    queue: VecDeque<(T, usize)>,
    distances: BTreeMap<usize, usize>,
}

impl<T: PartialOrd> MonotonicQueue<T> {
    pub fn new() -> MonotonicQueue<T> {
        MonotonicQueue {
            queue: VecDeque::new(),
            distances: BTreeMap::new(),
        }
    }

    fn push(self: &mut Self, item: T, idx: usize) {
        while self.queue.back().filter(|(last, _)| { &item >= last }).is_some() {
            self.queue.pop_back();
        }

        self.distances.insert(idx, self.queue.back().map(|(_, i)| { i - idx }).unwrap_or(0));
        self.queue.push_back((item, idx));
    }
}


pub fn daily_temperatures(temperatures: Vec<i32>) -> Vec<i32> {
    let mut queue = MonotonicQueue::new();

    for i in (0..temperatures.len()).rev() {
        queue.push(temperatures[i], i);
    }

    queue.distances.values().map(|&i| i as i32).into_iter().collect()
}

fn main() {}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn sample_test_1() {
        let temperatures = vec![73, 74, 75, 71, 69, 72, 76, 73];
        let expected = vec![1, 1, 4, 2, 1, 1, 0, 0];
        let temperatures = daily_temperatures(temperatures);
        assert_eq!(temperatures, expected);
    }

    #[test]
    fn sample_test_2() {
        let temperatures = vec![30, 40, 50, 60];
        let expected = vec![1, 1, 1, 0];
        let temperatures = daily_temperatures(temperatures);
        assert_eq!(temperatures, expected);
    }

    #[test]
    fn sample_test_3() {
        let temperatures = vec![30, 60, 90];
        let expected = vec![1, 1, 0];
        let temperatures = daily_temperatures(temperatures);
        assert_eq!(temperatures, expected);
    }
}

