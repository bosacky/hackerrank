use std::collections::{HashMap, HashSet, VecDeque};
use std::env;
use std::fs::File;
use std::io::{self, BufRead, Write};

/*
 * Complete the 'quickest_way_up' function below.
 *
 * The function is expected to return an INTEGER.
 * The function accepts following parameters:
 *  1. 2D_INTEGER_ARRAY ladders
 *  2. 2D_INTEGER_ARRAY snakes
 */

fn quickest_way_up(ladders: &[Vec<i32>], snakes: &[Vec<i32>]) -> i32 {
    let mut shortcuts = HashMap::new();

    for ladder in ladders {
        shortcuts.insert(ladder[0], ladder[1]);
    }

    for snake in snakes {
        shortcuts.insert(snake[0], snake[1]);
    }

    let mut queue: VecDeque<(i32, i32)> = VecDeque::new();
    let mut visited: HashSet<i32> = HashSet::new();
    queue.push_back((1, 0));

    let mut min_cost = -1;
    while !queue.is_empty() {
        let (pos, cost) = queue.pop_front().unwrap();
        if pos == 100 {
            min_cost = cost;
            break;
        }
        visited.insert(pos);
        for next in (1..=6)
            .map(|x| &pos + x)
            .filter(|x| *x <= 100)
            .filter(|x| !visited.contains(&x)) {
            let shortcut = *shortcuts.get(&next).unwrap_or(&next);
            queue.push_back((shortcut, cost + 1));
        }
    }

    min_cost
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn sample_test_1() {
        let ladders = vec![vec![32, 62], vec![42, 68], vec![12, 98]];
        let snakes = vec![vec![95, 13], vec![97, 25], vec![93, 37], vec![79, 27], vec![75, 19], vec![49, 47], vec![67, 17]];
        let result = quickest_way_up(&ladders, &snakes);
        assert_eq!(result, 3);
    }

    #[test]
    fn sample_test_2() {
        let ladders = vec![vec![8, 52], vec![6, 80], vec![26, 42], vec![2, 72]];
        let snakes = vec![vec![51, 19], vec![39, 11], vec![37, 29], vec![81, 3], vec![59, 5], vec![79, 23], vec![53, 7], vec![43, 33], vec![77, 21]];
        let result = quickest_way_up(&ladders, &snakes);
        assert_eq!(result, 5);
    }
}


fn main() {
    let stdin = io::stdin();
    let mut stdin_iterator = stdin.lock().lines();

    let mut fptr = File::create(env::var("OUTPUT_PATH").unwrap()).unwrap();

    let t = stdin_iterator.next().unwrap().unwrap().trim().parse::<i32>().unwrap();

    for _ in 0..t {
        let n = stdin_iterator.next().unwrap().unwrap().trim().parse::<i32>().unwrap();

        let mut ladders: Vec<Vec<i32>> = Vec::with_capacity(n as usize);

        for i in 0..n as usize {
            ladders.push(Vec::with_capacity(2_usize));

            ladders[i] = stdin_iterator.next().unwrap().unwrap()
                .trim_end()
                .split(' ')
                .map(|s| s.to_string().parse::<i32>().unwrap())
                .collect();
        }

        let m = stdin_iterator.next().unwrap().unwrap().trim().parse::<i32>().unwrap();

        let mut snakes: Vec<Vec<i32>> = Vec::with_capacity(m as usize);

        for i in 0..m as usize {
            snakes.push(Vec::with_capacity(2_usize));

            snakes[i] = stdin_iterator.next().unwrap().unwrap()
                .trim_end()
                .split(' ')
                .map(|s| s.to_string().parse::<i32>().unwrap())
                .collect();
        }

        let result = quickest_way_up(&ladders, &snakes);

        writeln!(&mut fptr, "{}", result).ok();
    }
}
