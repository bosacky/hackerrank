use std::collections::{HashMap, HashSet};
use std::env;
use std::fs::File;
use std::io::{self, BufRead, Write};
use std::iter::once;

#[derive(Eq, Hash, PartialEq)]
struct Road {
    from: i32,
    to: i32,
    cost: i32,
}

fn min_time(roads: &Vec<Vec<i32>>, machines: &Vec<i32>) -> i32 {
    let machines: HashSet<&i32> = machines.iter().collect();
    let roads: Vec<Road> = roads.iter().map(|v| Road { from: v[0], to: v[1], cost: v[2] }).collect();
    let mut roads: HashMap<&i32, HashSet<&Road>> = roads.iter()
        .fold(HashMap::new(), |mut acc, road| {
            acc.entry(&road.from)
                .and_modify(|set| { set.insert(&road); })
                .or_insert(vec![road].into_iter().collect());
            acc.entry(&road.to)
                .and_modify(|set| { set.insert(&road); })
                .or_insert(vec![road].into_iter().collect());
            acc
        });

    let mut min = 0;

    for &city in &machines {
        println!("Starting from {}", city);
        let mut stack: Vec<(&i32, Vec<&Road>)> = vec![];
        let mut visited: HashSet<&i32> = HashSet::new();
        stack.push((city, vec![]));

        while !stack.is_empty() {
            let (node, path) = stack.pop().unwrap();
            if node != city && machines.contains(node) {
                println!("There is machine {}", node);
                let min_road = path.iter().min_by(|r1, r2| r1.cost.cmp(&r2.cost)).unwrap();
                println!("Minimal road {} {} {}", min_road.from, min_road.to, min_road.cost);
                roads.get_mut(&min_road.from).unwrap().remove(min_road);
                roads.get_mut(&min_road.to).unwrap().remove(min_road);
                min += min_road.cost;
                stack.clear();
                visited.clear();
                stack.push((city, vec![]));
                println!("Cost so far {}", min);
            } else {
                println!("Visiting {}", node);
                visited.insert(node);
                for &road in roads.get(node).unwrap_or(&HashSet::new()).iter() {
                    let to = if road.from == *node { &road.to } else { &road.from };
                    if !visited.contains(&to) {
                        let new_path: Vec<&Road> = path.iter().cloned().chain(once(road)).collect();
                        stack.push((&to, new_path));
                    }
                }
            }
        }
        println!();
    }

    min
}

fn main() {
    let stdin = io::stdin();
    let mut stdin_iterator = stdin.lock().lines();

    let mut fptr = File::create(env::var("OUTPUT_PATH").unwrap()).unwrap();

    let first_multiple_input: Vec<String> = stdin_iterator.next().unwrap().unwrap()
        .split(' ')
        .map(|s| s.to_string())
        .collect();

    let n = first_multiple_input[0].trim().parse::<i32>().unwrap();

    let k = first_multiple_input[1].trim().parse::<i32>().unwrap();

    let mut roads: Vec<Vec<i32>> = Vec::with_capacity((n - 1) as usize);

    for i in 0..(n - 1) as usize {
        roads.push(Vec::with_capacity(3_usize));

        roads[i] = stdin_iterator.next().unwrap().unwrap()
            .trim_end()
            .split(' ')
            .map(|s| s.to_string().parse::<i32>().unwrap())
            .collect();
    }

    let mut machines: Vec<i32> = Vec::with_capacity(k as usize);

    for _ in 0..k {
        let machines_item = stdin_iterator.next().unwrap().unwrap().trim().parse::<i32>().unwrap();
        machines.push(machines_item);
    }

    let result = min_time(&roads, &machines);

    writeln!(&mut fptr, "{}", result).ok();
}


#[cfg(test)]
mod tests {
    use crate::min_time;

    #[test]
    fn sample_test_0() {
        let _n = 5;
        let _k = 3;
        let roads = vec![
            vec![2, 1, 8],
            vec![1, 0, 5],
            vec![2, 4, 5],
            vec![1, 3, 4]];
        let machines = vec![2, 4, 0];

        let result = min_time(&roads, &machines);
        assert_eq!(result, 10);
    }

    #[test]
    fn sample_test_1() {
        let roads = vec![
            vec![4, 6, 4],
            vec![6, 5, 4],
            vec![6, 1, 9],
            vec![5, 2, 5],
            vec![6, 7, 4],
            vec![1, 8, 3],
            vec![6, 0, 9],
            vec![8, 9, 10],
            vec![5, 3, 7]];
        let machines = vec![1, 2, 4, 9, 0, 7, 5, 3, 6, 8];
        let result = min_time(&roads, &machines);
        assert_eq!(result, 55);
    }

    #[test]
    fn test_case_10() {
        let roads = vec![
            vec![0, 1, 4],
            vec![1, 2, 3],
            vec![1, 3, 7],
            vec![0, 4, 2],
        ];
        let machines = vec![2, 3, 4];

        let result = min_time(&roads, &machines);
        assert_eq!(result, 5);
    }
}
