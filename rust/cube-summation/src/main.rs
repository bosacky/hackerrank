use std::collections::HashMap;
use std::{env};
use std::fs::File;
use std::io::{self, BufRead, Write};
use std::str::FromStr;
use crate::Operation::{QUERY, UPDATE};

#[derive(Debug, PartialEq, Clone, Copy, Eq, Hash)]
enum Operation {
    UPDATE { x: i32, y: i32, z: i32, v: i32 },
    QUERY { x1: i32, y1: i32, z1: i32, x2: i32, y2: i32, z2: i32 },
}

impl FromStr for Operation {
    type Err = ();

    fn from_str(input: &str) -> Result<Operation, Self::Err> {
        let split = input.trim().split(" ").collect::<Vec<_>>();
        let nums: Vec<i32> = split[1..].into_iter().map(|s| s.to_string().parse().unwrap()).collect();
        match split[0] {
            "UPDATE" => {
                Ok(UPDATE { x: nums[0], y: nums[1], z: nums[2], v: nums[3] })
            }
            "QUERY" => {
                Ok(QUERY { x1: nums[0], y1: nums[1], z1: nums[2], x2: nums[3], y2: nums[4], z2: nums[5] })
            }
            _ => Err(()),
        }
    }
}

fn cube_sum(_n: i32, operations: &Vec<Operation>) -> Vec<i64> {
    let mut map: HashMap<(i32, i32, i32), i64> = HashMap::new();
    let mut sums: Vec<i64> = vec![];

    for op in operations {
        match op {
            QUERY { x1, y1, z1, x2, y2, z2 } => {
                let sum = map.iter()
                    .filter(|((x, y, z), _)| {
                        *x >= *x1 && *x <= *x2 && *y >= *y1 && *y <= *y2 && *z >= *z1 && *z <= *z2
                    }).map(|(_, v)| v).sum();
                sums.push(sum);
            }
            UPDATE { x, y, z, v } => {
                map.insert((*x, *y, *z), *v as i64);
            }
        }
    }

    sums
}

fn parse_lines<T: BufRead>(lines: &mut io::Lines<T>) -> Vec<(i32, Vec<Operation>)> {
    let t = lines.next().unwrap().unwrap().trim().parse::<i32>().unwrap();

    let output: Vec<(i32, Vec<Operation>)> = (0..t).into_iter().map(|_| {
        let first_multiple_input: Vec<_> = lines.next().unwrap().unwrap()
            .split(' ')
            .map(|s| s.to_string())
            .collect();

        let n = first_multiple_input[0].trim().parse::<i32>().unwrap();
        let m = first_multiple_input[1].trim().parse::<i32>().unwrap();

        let ops: Vec<Operation> = (0..m).into_iter().flat_map(|_| {
            Operation::from_str(&lines.next().unwrap().unwrap())
        }).collect();

        (n, ops)
    }).collect();

    output
}

fn main() {
    let stdin = io::stdin();
    let mut stdin_iterator = stdin.lock().lines();
    let mut fptr = File::create(env::var("OUTPUT_PATH").unwrap()).unwrap();

    for (n, ops) in parse_lines(&mut stdin_iterator) {
        let res = cube_sum(n, &ops);

        for i in 0..res.len() {
            write!(&mut fptr, "{}", res[i]).ok();

            if i != res.len() - 1 {
                writeln!(&mut fptr).ok();
            }
        }

        writeln!(&mut fptr).ok();
    }
}

#[cfg(test)]
mod tests {
    use std::fs::File;
    use std::io::{BufRead, BufReader, Lines};
    use crate::*;
    use super::cube_sum;

    #[test]
    fn test_0a() {
        let n = 4;

        let operations: Vec<Operation> = vec![
            "UPDATE 2 2 2 4",
            "QUERY 1 1 1 3 3 3",
            "UPDATE 1 1 1 23",
            "QUERY 2 2 2 4 4 4",
            "QUERY 1 1 1 3 3 3"]
            .iter().flat_map(|s| Operation::from_str(s)).collect();

        let actual = cube_sum(n, &operations);
        let expected = vec![4, 4, 27];
        assert_eq!(expected, actual);
    }

    #[test]
    fn test_0b() {
        let n = 2;

        let operations: Vec<Operation> = vec![
            "UPDATE 2 2 2 1",
            "QUERY 1 1 1 1 1 1",
            "QUERY 1 1 1 2 2 2",
            "QUERY 2 2 2 2 2 2"]
            .iter().flat_map(|s| Operation::from_str(s)).collect();
        let actual = cube_sum(n, &operations);
        let expected = vec![0, 1, 1];
        assert_eq!(expected, actual);
    }

    fn file_lines(s: &str) -> Lines<BufReader<File>> {
        let file = File::open(s).unwrap();
        let reader = BufReader::new(file);
        reader.lines()
    }

    #[test]
    fn test_0() {
        let mut input_lines = file_lines("resources/test/input0.txt");
        let parsed = parse_lines(&mut input_lines);
        let actual: Vec<i64> = parsed.iter().flat_map(|(n, ops)| cube_sum(*n, ops)).collect();

        let output_lines = file_lines("resources/test/output0.txt");
        let expected: Vec<i64> = output_lines.map(|l| l.unwrap().parse::<i64>().unwrap()).collect();

        assert_eq!(expected, actual);
    }

    #[test]
    fn test_1() {
        let mut input_lines = file_lines("resources/test/input1.txt");
        let parsed = parse_lines(&mut input_lines);
        let actual: Vec<i64> = parsed.iter().flat_map(|(n, ops)| cube_sum(*n, ops)).collect();

        let output_lines = file_lines("resources/test/output1.txt");
        let expected: Vec<i64> = output_lines.map(|l| l.unwrap().parse::<i64>().unwrap()).collect();

        assert_eq!(expected, actual);
    }
}