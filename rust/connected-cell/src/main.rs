use std::cmp::max;
use std::collections::HashSet;
use std::env;
use std::fs::File;
use std::io::{self, BufRead, Write};

fn max_region(grid: &Vec<Vec<i32>>) -> i32 {
    let n = grid.len();
    let m = grid.get(0).unwrap().len();

    let mut visited: HashSet<(usize, usize)> = HashSet::new();

    let mut count = 0;

    for i in 0..n {
        for j in 0..m {
            if !visited.contains(&(i, j)) {
                let mut stack: Vec<(usize, usize)> = vec![];
                if grid[i][j] == 1 {
                    stack.push((i, j));
                }

                let mut count_in_run = 0;
                while !stack.is_empty() {
                    let node = stack.pop().unwrap();
                    if !visited.contains(&node) {
                        println!("Visiting {} {}", node.0, node.1);
                        count_in_run += 1;
                        visited.insert(node);

                        for x in -1..=1 {
                            for y in -1..=1 {
                                let u = Some(node.0 as i32 + x).filter(|&u| { u >= 0 && u < n as i32 }).map(|u| u as usize);
                                let v = Some(node.1 as i32 + y).filter(|&v| { v >= 0 && v < m as i32 }).map(|v| v as usize);

                                if u.is_some() && v.is_some() && (u.unwrap(), v.unwrap()) != (i, j)
                                    && !visited.contains(&(u.unwrap(), v.unwrap())) && grid[u.unwrap()][v.unwrap()] == 1 {
                                    stack.push((u.unwrap(), v.unwrap()));
                                }
                            }
                        }
                    }
                }
                println!();
                count = max(count_in_run, count);
            }
        }
    }

    count
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn sample_test_0() {
        let grid = vec![
            vec![1, 1, 0, 0],
            vec![0, 1, 1, 0],
            vec![0, 0, 1, 0],
            vec![1, 0, 0, 0]];
        let result = max_region(&grid);
        assert_eq!(result, 5);
    }

    #[test]
    fn sample_test_1() {
        let grid = vec![
            vec![0, 0, 1, 1],
            vec![0, 0, 1, 0],
            vec![0, 1, 1, 0],
            vec![0, 1, 0, 0],
            vec![1, 1, 0, 0]];
        let result = max_region(&grid);
        assert_eq!(result, 8);
    }
}

fn main() {
    let stdin = io::stdin();
    let mut stdin_iterator = stdin.lock().lines();

    let mut fptr = File::create(env::var("OUTPUT_PATH").unwrap()).unwrap();

    let n = stdin_iterator.next().unwrap().unwrap().trim().parse::<i32>().unwrap();

    let m = stdin_iterator.next().unwrap().unwrap().trim().parse::<i32>().unwrap();

    let mut grid: Vec<Vec<i32>> = Vec::with_capacity(n as usize);

    for i in 0..n as usize {
        grid.push(Vec::with_capacity(m as usize));

        grid[i] = stdin_iterator.next().unwrap().unwrap()
            .trim_end()
            .split(' ')
            .map(|s| s.to_string().parse::<i32>().unwrap())
            .collect();
    }

    let res = max_region(&grid);

    writeln!(&mut fptr, "{}", res).ok();
}
