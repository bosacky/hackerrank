use std::env;
use std::fs::File;
use std::io::{self, BufRead, Write};

struct Skyscraper {
    height: i32,
    count: i32,
}

impl Skyscraper {
    pub const fn new(height: i32) -> Self { Skyscraper { height, count: 1 } }

    pub fn increase_count(self: &mut Self) {
        self.count += 1
    }
}

fn solve(heights: &[i32]) -> i64 {
    let mut count: i64 = 0;

    let mut mono_stack: Vec<Skyscraper> = vec![];
    for &height in heights {
        while mono_stack.last().filter(|l| l.height < height).is_some() { mono_stack.pop(); }

        match mono_stack.last() {
            Some(Skyscraper { height: h, count: c }) if *h == height => {
                count += *c as i64;
                mono_stack.last_mut().unwrap().increase_count()
            }
            _ => { mono_stack.push(Skyscraper::new(height)) }
        }
    }

    count * 2
}

#[cfg(test)]
mod tests {
    use std::io::BufReader;
    use super::*;

    #[test]
    fn sample_test_1() {
        let arr = [3, 2, 1, 2, 3, 3];
        let result = solve(&arr);
        assert_eq!(result, 8);
    }

    #[test]
    fn sample_test_2() {
        let arr = [1, 1000, 1];
        let result = solve(&arr);
        assert_eq!(result, 0);
    }

    #[test]
    fn test_14() {
        let count = 300000;
        let expected: i64 = 89999700000;
        let arr = vec![1; count];
        let result = solve(&arr);
        assert_eq!(result, expected);
    }

    #[test]
    fn test_9() {
        let filename = "resources/test/input09.txt";
        let file = File::open(filename).unwrap();
        let reader = BufReader::new(file);
        let mut lines = reader.lines();

        let arr = parse_lines(&mut lines);
        let output = solve(&arr);

        let filename = "resources/test/output09.txt";
        let file = File::open(filename).unwrap();
        let reader = BufReader::new(file);
        let mut lines = reader.lines();
        let expected = lines.next().unwrap().unwrap().trim().parse::<i64>().unwrap();
        assert_eq!(expected, output)
    }
}

fn parse_lines<T: BufRead>(lines: &mut io::Lines<T>) -> Vec<i32> {
    let _arr_count = lines.next().unwrap().unwrap().trim().parse::<i32>().unwrap();
    lines.next().unwrap().unwrap()
        .trim_end()
        .split(' ')
        .map(|s| s.to_string().parse::<i32>().unwrap())
        .collect()
}

fn main() {
    let stdin = io::stdin();
    let mut stdin_iterator = stdin.lock().lines();
    let mut fptr = File::create(env::var("OUTPUT_PATH").unwrap()).unwrap();
    let arr = parse_lines(&mut stdin_iterator);
    let result = solve(&arr);
    writeln!(&mut fptr, "{}", result).ok();
}
