use std::env;
use std::fs::File;
use std::io::{self, BufRead, Write};

/*
 * Complete the 'truckTour' function below.
 *
 * The function is expected to return an INTEGER.
 * The function accepts 2D_INTEGER_ARRAY petrolpumps as parameter.
 */

fn truck_tour(petrolpumps: Vec<Vec<i32>>) -> i32 {
    let mut first_index = 0;
    let mut gas_left = 0;
    for (i, v) in petrolpumps.iter().enumerate() {
        if let [gas_available, gas_needed] = v.as_slice() {
            let gas_left_after = gas_left + gas_available - gas_needed;
            if gas_left_after < 0 {
                gas_left = 0;
                first_index = i + 1;
            } else {
                gas_left = gas_left_after;
            }
        } else { panic!("Invalid input. {:?}", v) }
    }

    first_index as i32
}

fn parse_lines<T: BufRead>(lines: &mut io::Lines<T>) -> Vec<Vec<i32>> {
    let n = lines.next().unwrap().unwrap().trim().parse::<i32>().unwrap();

    let mut petrolpumps: Vec<Vec<i32>> = Vec::with_capacity(n as usize);

    for i in 0..n as usize {
        petrolpumps.push(Vec::with_capacity(2_usize));

        petrolpumps[i] = lines.next().unwrap().unwrap()
            .trim_end()
            .split(' ')
            .map(|s| s.to_string().parse::<i32>().unwrap())
            .collect();
    }

    petrolpumps
}

fn main() {
    let stdin = io::stdin();
    let mut stdin_iterator = stdin.lock().lines();
    let mut fptr = File::create(env::var("OUTPUT_PATH").unwrap()).unwrap();

    let petrolpumps = parse_lines(&mut stdin_iterator);
    let result = truck_tour(petrolpumps);
    writeln!(&mut fptr, "{}", result).ok();
}


#[cfg(test)]
mod tests {
    use std::fs::File;
    use std::io::{BufRead, BufReader};
    use crate::{parse_lines, truck_tour};

    #[test]
    fn sample_test_0() {
        let petrolpumps = vec![
            vec![1, 5],
            vec![10, 3],
            vec![3, 4]];
        let actual = truck_tour(petrolpumps);
        let expected = 1;
        assert_eq!(expected, actual)
    }

    #[test]
    fn test_1() {
        let filename = "resources/test/input01.txt";
        let file = File::open(filename).unwrap();
        let reader = BufReader::new(file);
        let mut lines = reader.lines();

        let arr = parse_lines(&mut lines);
        let output = truck_tour(arr);

        let filename = "resources/test/output01.txt";
        let file = File::open(filename).unwrap();
        let reader = BufReader::new(file);
        let mut lines = reader.lines();
        let expected = lines.next().unwrap().unwrap().trim().parse::<i32>().unwrap();
        assert_eq!(expected, output)
    }
}